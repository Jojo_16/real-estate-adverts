<?php

namespace App\Repository;

use App\Entity\Property;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Property>
 *
 * @method Property|null find($id, $lockMode = null, $lockVersion = null)
 * @method Property|null findOneBy(array $criteria, array $orderBy = null)
 * @method Property[]    findAll()
 * @method Property[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Property::class);
    }

    /**
     * @param string $userPrice
     * @param string $userRooms
     * 
     * @return Query
     */
    public function findFilteredQuery(string $userPrice, string $userRooms, string $userOption): Query
    {
        $qb = $this->findVisibleQuery();
        if ($userPrice !== "") {
            $qb->andWhere("p.price <= $userPrice");
        }
        if ($userRooms !== "") {
            $qb->andWhere("p.rooms <= $userRooms");
        }
        if ($userOption !== "") {
            $qb->join("p.options", "o");
            $qb->andWhere("o.id = $userOption");
        }
        return $qb->getQuery();
    }

    /**
     * @return Property[]
     */
    public function findLatest(): array
    {
        return $this->findVisibleQuery()
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Object
     */
    public function findVisibleQuery()
    {
        return $this->createQueryBuilder('p')
            ->where('p.sold = false');
    }
}
