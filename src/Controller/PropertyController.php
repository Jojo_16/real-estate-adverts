<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Property;
use App\Form\ContactType;
use App\Controller\ContactController;
use App\Repository\OptionRepository;
use App\Repository\PropertyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PropertyController extends AbstractController
{

  private $repository;
  private $entity;

  public function __construct(PropertyRepository $repository, EntityManagerInterface $entity)
  {
    $this->repository = $repository;
    $this->entity = $entity;
  }


  #[Route('/biens', name: 'property_index')]
  public function index(OptionRepository $optionRepository, PaginatorInterface $paginator, Request $request): Response
  {
    $userPrice = "";
    $userRooms = "";
    $userOptions = [];

    if ($this->isCsrfTokenValid('search', $request->get('_token'))) {
      $userPrice = $request->get('userPrice');
      $userRooms = $request->get('userRooms');
      $userOptions = $request->get('userOptions');
      $query = $this->repository->findFilteredQuery($userPrice, $userRooms, $userOptions);
    } else {
      $query = $this->repository->findVisibleQuery()->getQuery();
    }

    $optionList = $optionRepository->findAll();
    $properties = $paginator->paginate(
      $query,
      $request->query->getInt('page', 1),
      12
    );

    return $this->render('property/index.html.twig', [
      'current_menu' => 'properties',
      'properties' => $properties,
      'user_price' => $userPrice,
      'user_rooms' => $userRooms,
      'user_options' => $userOptions,
      'option_list' => $optionList
    ]);
  }

  #[Route('/biens/{slug}-{id}', name: 'property_show', requirements: ['slug' => "^[a-z]+(?:-[a-z]+)*$"])]
  public function show(Property $property, string $slug, Request $request, ContactController $notification): Response
  {
    
    if ($property->getSlug() !== $slug) {
      return $this->redirectToRoute('property_show', [
        'id' => $property->getId(),
        'slug' => $property->getSlug()
      ], 301);
    }
    
    $contact = new Contact();
    $contact->setProperty($property);
    $form = $this->createForm(ContactType::class, $contact);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $notification->notify($contact);
      $this->addFlash('success', 'Votre email abien été envoyé');
      return $this->redirectToRoute('property_show', [
        'id' => $property->getId(),
        'slug' => $property->getSlug()
      ]);
    }

    return $this->render('property/show.html.twig', [
      'property' => $property,
      'current_menu' => 'properties',
      'form' => $form->createView()
    ]);
  }
}
