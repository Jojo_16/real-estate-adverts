<?php

namespace App\Controller\Admin;

use App\Entity\Property;
use App\Form\PropertyType;
use App\Repository\PropertyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminPropertyController extends AbstractController
{

  private $repository;
  private $em;

  public function __construct(PropertyRepository $repository, EntityManagerInterface $em)
  {
    $this->repository = $repository;
    $this->em = $em;
  }

  #[Route('/admin/prop', name: 'admin_property_index')]
  public function index(PropertyRepository $repository): Response
  {
    $properties = $this->repository->findAll();
    return $this->render('admin/property/index.html.twig', compact('properties'));
  }

  #[Route('/admin/property/create', name: 'admin_property_new')]
  public function new(Request $request)
  {
    $property = new Property();
    $form = $this->createForm(PropertyType::class, $property);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $this->em->persist($property);
      $this->em->flush();
      $this->addFlash('success', 'Créé avec succès !');
      return $this->redirectToRoute('admin_property_index');
    }

    return $this->render('admin/property/new.html.twig', [
      'property' => $property,
      'form' => $form->createView()
    ]);
  }

  #[Route('/admin/property/{id}', name: 'admin_property_delete', methods: ['DELETE'])]
  public function delete(Property $property, Request $request): Response
  {
    if ($this->isCsrfTokenValid('delete' . $property->getId(), $request->get('_token'))) {
      $this->em->remove($property);
      $this->em->flush();
      $this->addFlash('success', 'Supprimé avec succès !');
    }
    return $this->redirectToRoute('admin_property_index');
  }

  #[Route('/admin/property/{id}', name: 'admin_property_edit', methods: ['GET', 'POST'])]
  public function edit(Property $property, Request $request)
  {
    $form = $this->createForm(PropertyType::class, $property);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $this->em->flush();
      $this->addFlash('success', 'Modifié avec succès !');
      return $this->redirectToRoute('admin_property_index');
    }

    return $this->render('admin/property/edit.html.twig', [
      'property' => $property,
      'form' => $form->createView()
    ]);
  }
}
