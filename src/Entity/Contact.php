<?php 
namespace App\Entity;

use Symfony\Component\Validator\Test\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

class Contact {

    #[ORM\Column(length: 255)]
    #[Assert\Length(min: 2)]
    private ?string $firstname = null;

    #[ORM\Column(length: 255)]
    #[Assert\Length(min: 2)]
    private ?string $lastname = null;

    #[ORM\Column]
    #[Assert\Regex("/^[0-9]{10}$/")]
    private ?string $phone = null;

    #[ORM\Column]
    #[Assert\Email()]
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    #[Assert\Length(min: 10)]
    private ?string $message = null;

    #[ORM\Column]
    private ?Property $property = null;

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): Contact
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): Contact
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): Contact
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): Contact
    {
        $this->email = $email;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): Contact
    {
        $this->message = $message;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): Contact
    {
        $this->property = $property;

        return $this;
    }
}