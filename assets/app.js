/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

console.log('Hello webpack Encore !!');
// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';
import $ from 'jquery';
import 'select2';

$('select').select2();
let $contactButton = $('#contactButton');

$contactButton.on("click", e => {
    e.preventDefault()
    $('#contactForm').slideDown();
    $contactButton.slideUp();
})